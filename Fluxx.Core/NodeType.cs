﻿using System;
namespace Fluxx.Core
{
	public enum NodeType
	{
		//Terminal nodes
		Empty,
		Number,
		Integer,
		Bool,
		String,
		//Reference node
		Id,
		Param,
		Extract,
		//Unary arithmetic operators
		Minus,
		Plus,
		//Arithmetic operators
		Add,
		Sub,
		Mul,
		Div,
		Mod,
		Pow,
		//Boolean operators
		And,
		Or,
		Not,
		Equal,
		Greater,
		Less,
		//Containers
		Container,
		Parents,
		//Call operators
		Blank,
		Arrow,
		Input,
		//Statements
		Cond,
		Range,
		Set,
		//Miscellanous
		Unpack,
		Collection,
		//Special
		Bind
	}
}
