﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.Security.Cryptography;

namespace Fluxx.Core
{

	[Serializable]
	public class Node
	{

		public event Action<Node, object> ValueChanged;
		
		private string _name;

		public string Name
		{
			get { return _name; }
			set 
			{ 
				_name = value;
				IsAnonymous = false;
			}
		}

		[XmlIgnore]
		public Node Scope { get; set; }
		
		public string Text { get; set; }

		private object _value;
		
		public object Value 
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
				if (ValueChanged != null)
					ValueChanged(this, _value);
			}
		}
		
		public NodeType Type { get; set; }
		
		[XmlIgnore]
		public Node Parent { get; private set; }

		public List<Node> children { get; set; }
		
		public IEnumerable<Node> Children
		{
			get
			{
				return children.AsEnumerable();
			}
		}
		
		[XmlIgnore]
		public Node this[int index]
		{
			get { return children.ElementAt(index); }
		}

		private Dictionary<string, Node> _variables;
		
		public bool AddVar(Node n)
		{
			return AddVar(n.Name, n);
		}
		
		public bool AddVar(string name, Node n)
		{
			if (_variables.ContainsKey(name))
				return false;
			
			_variables.Add(name, n);
			return true;			
		}
		
		public Node GetVar(string name)
		{
			if (_variables.ContainsKey(name))
				return _variables[name];
			else if (ReceivedNode._variables.ContainsKey(name))
				return ReceivedNode._variables[name];
			else
				return null;

			/*
			if (_variables.ContainsKey(name))
				return _variables[name];
			else
				throw new Exception("variable x is not defined");
			*/
			/*return null;*/
		}
		
		public void RemoveVar(string name)
		{
			if (_variables.ContainsKey(name))
				_variables.Remove(name);
		}
		
		public IEnumerable<Node> GetExpressions()
		{
			return ReceivedNode.Children;
		}

		public Node GetExpression(int index)
		{			
			return ReceivedNode[index];
		}

		public int CountExpression()
		{
			return ReceivedNode.Children.Count();
		}
		
		[XmlIgnore]
		public Node this[string name]
		{
			get { return GetVar(name); }
		}

		//[XmlIgnore]
		//public Context Context { get; set; }
		[XmlIgnore]
		public Node ReceivedNode { get; set; }

		[XmlIgnore]
		public bool IsAnonymous { get; private set; }

		public bool IsContainer
		{
			get
			{
				return Type == NodeType.Container;
			}
		}

		public bool IsEmpty
		{
			get { return Type == NodeType.Empty; }
		}

		public int Arity
		{
			get
			{
				return children.Count;
			}
		}

		public bool IsTerminal
		{
			get
			{
				return Arity == 0 && !IsContainer;
			}
		}

		public bool IsUnary
		{
			get
			{
				return Arity == 1;
			}
		}

		public bool IsBinary
		{
			get
			{
				return Arity == 2;
			}
		}

		public Node() : this(null) {}
		
		public Node(Node scope) 
		{
			Name = "anonym_" + this.GetHashCode();
			//Context = new Context();
			children = new List<Node>();
			IsAnonymous = true;
			Scope = scope;
			_variables = new Dictionary<string, Node>();
		}
		
		public Node(NodeType type) : this(type, null) {}

		public Node(NodeType type, Node scope) : this(scope)
		{
			Type = type;
		}

		public void Add(Node n)
		{
			if (n == null)
				return;
			
			children.Add(n);
			n.Parent = this;
		}
		
		public void AddRange(IEnumerable<Node> nodes)
		{
			foreach (var n in nodes)
				Add(n);
		}

		public void Remove(Node n)
		{
			children.Remove(n);
		}

		public void RemoveRange(IEnumerable<Node> nodes)
		{
			foreach (var n in nodes)
				children.Remove(n);
		}

		public void ReceiveContextFrom(Node n)
		{
			//Only containers can receive context
			//Check every right children for send the context
			/*
			if (this.Type != NodeType.CONTAINER)
			{
				if (this.Children.Count() == 0)
					return;
				
				this[1].ReceiveContextFrom(n);
			}


			foreach (var kv in n.Context.variables)
			{
				if (!this.Context.variables.ContainsKey(kv.Key))
					this.Context.variables.Add(kv.Key, kv.Value);
			}

			this.Context.expressions.Clear();
			this.Context.expressions.InsertRange(0, n.Children);
			*/
			ReceivedNode = n;
		}

		
		public Node ShallowCopy()
		{
			Node cpy = DataCopy();
			cpy.children = children;
			return cpy;
		}

		public Node DataCopy()
		{
			Node cpy = new Node(Type);
			cpy.Name = Name;
			//cpy.Context = Context;
			cpy.ReceivedNode = ReceivedNode;
			cpy._variables = _variables;
			cpy.Value = Value;
			cpy.Text = Text;
			cpy.Parent = Parent;
			cpy.Scope = Scope;
			return cpy;
		}

		public void Reverse()
		{
			children.Reverse();
		}

		public string GetSyntaxTree()
		{
			XmlSerializer xs = new XmlSerializer(typeof(Node));
			using (StringWriter sw = new StringWriter())
			{
				xs.Serialize(sw, this);
				return sw.ToString();
			}
		}
		
		public override string ToString()
		{
			if (IsTerminal)
			{
				if (IsEmpty)
					return "_";
				
				if (Type != NodeType.String)
					return this.Value.ToString();
				
				return String.Format("'{0}'", this.Value.ToString());
			}
			
			if (Type == NodeType.Container)
				return String.Format("[{0}]", String.Join(",", this.Children.Select(x => x.ToString())));
			
			if (Type == NodeType.Parents)
				return String.Format("({0})", this[0].ToString());

			if (Type == NodeType.Add)
				return String.Format("{0}+{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Sub)
				return String.Format("{0}-{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Mul)
				return String.Format("{0}*{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Div)
				return String.Format("{0}/{1}", this[0].ToString(), this[1].ToString());

			if (Type == NodeType.Mod)
				return String.Format("{0}%{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Greater)
				return String.Format("{0}>{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Less)
				return String.Format("{0}<{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.And)
				return String.Format("{0} and {1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Or)
				return String.Format("{0} or {1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Range)
				return String.Format("{0}..{1}", this[0].ToString(), this[1].ToString());

			if (Type == NodeType.Blank)
				return String.Format("{0} ({1})", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Arrow)
				return String.Format("{0}->{1}", this[0].ToString(), this[1].ToString());
			
			if (Type == NodeType.Equal)
				return String.Format("{0}=={1}", this[0].ToString(), this[1].ToString());

			if (Type == NodeType.Set)
				return String.Format("{0} for {1} in {2}", this[0].ToString(), this[1].ToString(), this[2].ToString());
			
			
			return "";
		}
	
	}

}
