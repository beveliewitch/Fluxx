﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Fluxx.Core;

namespace Fluxx.Runtime
{
    public static class ReferenceImporter
    {
        
        public static IDictionary<string, Delegate> LoadModule(string path)
        {
            Dictionary<string, Delegate> refs = new Dictionary<string, Delegate>();
            
            Assembly module = Assembly.LoadFile(path);
            Type[] types = module.GetTypes();

            foreach (var type in types)
            {
                IDictionary<string, Delegate> classRefs = ImportReferences(type);
                foreach (var classRef in classRefs)
                {
                    refs.Add(classRef.Key, classRef.Value);
                }
            }

            return refs;
        }

        private static IDictionary<string, Delegate> ImportReferences(Type type)
        {
            Dictionary<string, Delegate> refs = new Dictionary<string, Delegate>();
            
            string name = type.Name;
            var methods = type.GetMethods(BindingFlags.Public | BindingFlags.Static);

            foreach (var method in methods)
            {
                var tuple = ImportReference(method);
                refs.Add(tuple.Item1, tuple.Item2);
            }

            return refs;
        }

        private static Tuple<string, Delegate> ImportReference(MethodInfo mi)
        {
            string name = mi.Name;
            var del =  (Func<Node, Node>)mi.CreateDelegate(typeof(Func<Node, Node>));
            return Tuple.Create<string, Delegate>(name, del);
        }
        
    }
}