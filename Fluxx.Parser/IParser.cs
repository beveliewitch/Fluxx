﻿using Fluxx.Core;

namespace Fluxx.Parser
{
    public interface IParser
    {
        Config Config { get; }
        Preprocessor Preprocessor { get; }
        ParserHelper ParserHelper { get; }

        Node Parse(string program);
    }
}