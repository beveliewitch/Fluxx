﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Fluxx.Core;

namespace Fluxx.Parser
{
    public class GrammarRule
    {
        private readonly string[] _tokens;
        private readonly ParserHelper _parserHelper;
        public string Lexeme { get; }
        public string Rule { get; }
        
        public GrammarRule(string lexeme, string rule, ParserHelper parserHelper)
        {
            Lexeme = lexeme;
            Rule = rule;
            _tokens = Regex.Split(rule, "[ \t]+");
            this._parserHelper = parserHelper;
        }

        public IEnumerable<string> Match(string expression)
        {
            var chunks = new List<string>();
            var remainder = expression;
            
            for (var i = 0; i < _tokens.Length; i++)
            {
                var token = _tokens[i];
                var nToken = i+1 < _tokens.Length ? _tokens[i + 1] : null;

                if (token == "<any>" || token == "<statement>")
                {
                    var lastIndexOf = remainder.Length;

                    if (nToken != null)
                    {
                        lastIndexOf = token == "<any>" ? remainder.IndexOf(nToken) : remainder.LastIndexOf(nToken);
                    }
                    
                    
                    if (lastIndexOf == -1)
                        return null;

                    //todo split ? 
                    if (nToken != null && !_parserHelper.IsApplicableSplit(expression, lastIndexOf, lastIndexOf + nToken.Length - 1))
                        return null;
                    
                    var statement = remainder.Substring(0, lastIndexOf).Trim();
                    
                    
                    
                    remainder = remainder.Remove(0, lastIndexOf);
                    chunks.Add(statement);
                    continue;
                }
                
                var regex = GetTokenRegex(token);
                var m = Regex.Match(remainder, regex);

                if (!m.Success || m.Index > 0)
                    return null;

                var index = m.Index;
                var length = m.Length;
                var capture = m.Captures[0].Value.Trim();

                remainder = remainder.Remove(index, length).Trim();
                
                //test
                if (token.StartsWith("<") && token.EndsWith(">"))
                    chunks.Add(capture);
            }

            return chunks;
        }

        private string GetTokenRegex(string token)
        {
            switch (token)
            {
                    case "<id>":
                        return @"[_a-zA-Z][_a-zA-Z0-9]{0,30}";
                    case "<number>":
                        return @"[0-9,\.]+";
                    case "<bool>":
                        return @"(true|false)";
                    case "<statement>":
                        return @"(.*)";
                    default:
                        return token;
            }
        }
        
    }
}