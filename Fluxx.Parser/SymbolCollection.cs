﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace Fluxx.Parser
{
	public class SymbolCollection : IEnumerable<Symbol>
	{

		private readonly Dictionary<Tuple<string, SymbolType>, Symbol> _collection;

		public Symbol this[Tuple<string, SymbolType> key] => _collection[key];

		public SymbolCollection()
		{
			_collection = new Dictionary<Tuple<string, SymbolType>, Symbol>();
		}

		public void Add(Symbol s)
		{
			_collection.Add(Tuple.Create(s.Lexeme, s.Type), s);
		}

		public IEnumerator<Symbol> GetEnumerator()
		{
			return _collection.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
