﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Fluxx.Core;

namespace Fluxx.Parser
{
	public struct Symbol
	{
		public string Lexeme { get; set; }
		public string Regex { get; set; }
		public Regex CompiledRegex { get; set; }
		public string Left { get; set; }
		public string Right { get; set; }
		public Assoc Assoc { get; set; }
		public int Precedence { get; set; }
		public SymbolType Type { get; set; }
		public NodeType NodeType { get; set; }

		public override string ToString()
		{
			return Lexeme;
		}

	}

	public enum SymbolType
	{
		Container,
		UnaOp,
		BinOp,
		Terminal
	}
	
	public enum Assoc
	{
		Left = -1,
		Right = 1
	}
	
	public struct PrecedenceGroup
	{
		public int Value { get; private set; }
		public IEnumerable<Symbol> Symbols { get; private set; }

		public PrecedenceGroup(int val, params Symbol[] symbols)
		{
			Symbols = symbols;
			Value = val;
		}

	}

}
