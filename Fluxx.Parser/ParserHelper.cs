﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;

namespace Fluxx.Parser
{
    public class ParserHelper
    {
        
        private const string STRING_DEL = "'";
		private Config _config;
        
        public ParserHelper(Config config)
        {
            _config = config;
        }
        
        public bool IsSurroundBy(string expression, Symbol container)
        {
            if (!(expression.StartsWith(container.Left, StringComparison.InvariantCulture) && 
                  expression.EndsWith(container.Right, StringComparison.InvariantCulture)))
                return false;

            int level = 0;

            for (var i = 0; i < expression.Length; i++)
            {
                char c = expression[i];
                string sc = c.ToString();

                //Change level
                if (sc == container.Left)
                    level++;
                else if (sc == container.Right)
                    level--;

                if (i != expression.Length - 1 && level == 0)
                    return false;
            }

            return (level == 0);
        }
        
        public List<BinarySplitObject> BinarySplits(string expression, IEnumerable<Symbol> separators)
        {
            List<BinarySplitObject> list = new List<BinarySplitObject>();

            foreach (Symbol separator in separators)
            {
                Regex regex = new Regex(separator.Regex);
                Match m = regex.Match(expression);

                while (m.Success)
                {
                    int startIndex = m.Index;
                    int endIndex = m.Index + m.Length;
                    bool isApplicable = IsApplicableSplit(expression, startIndex, endIndex);

                    if (isApplicable)
                    {
                        string leftStr = expression.Substring(0, startIndex).Trim();
                        string rightStr = expression.Substring(endIndex).Trim();

                        BinarySplitObject so = new BinarySplitObject() 
                        { 
                            Left = leftStr, 
                            Right = rightStr, 
                            Separator = separator, 
                            Index = startIndex, 
                            Length = m.Length 
                        };

                        list.Add(so);
                    }

                    m = m.NextMatch();
                }
            }

            //Order by the first finded to the last
            return list.OrderBy(x => x.Index).ToList();
        }

        public bool IsApplicableSplit(string expression, int startIndex, int endIndex)
        {
            //Parsing state machine (encapsulation for no side effects)
            var parsingState = GetParsingState();

            for (int i = 0; i < endIndex; i++)
            {
                char c = expression[i];
                string sc = c.ToString();
                bool canSplit = parsingState(sc);

                if (i >= startIndex && i < endIndex && !canSplit)
                    return false;
            }

            return true;
        }

        private Func<string, bool> GetParsingState()
        {
            int[] lvl = new int[_config.Containers.Count()];
            bool inString = false;

            return (s) =>
            {
                for (var i = 0; i < _config.Containers.Count(); i++)
                {
                    var container = _config.Containers.ElementAt(i);
					
                    if (s == container.Left)
                        lvl[i]++;
                    else if (s == container.Right)
                        lvl[i]--;
                }
                
                if (s == STRING_DEL)
                    inString = !inString;

                return (lvl.All(x => x == 0) && !inString);
            };
        }
        
        public IEnumerable<string> SplitDeclarations(string expression)
        {
            int lineNumber = 0;

            //Split declarations
            var splitObjects = BinarySplits(expression, new Symbol[] { new Symbol() { Regex = "," }});

            if (splitObjects.Count == 0)
                yield return expression;

            int lastIndex = 0;
            for (var i = 0; i < splitObjects.Count; i++)
            {
                var splitObject = splitObjects[i];

                string extract = splitObject.Left.Substring(lastIndex, splitObject.Index - lastIndex);
                yield return extract;
				
                if (i == splitObjects.Count - 1)
                    yield return splitObject.Right;

                lastIndex = splitObject.Index + 1;
            }
        }
        
    }
}